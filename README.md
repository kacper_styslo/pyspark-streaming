## SETUP

### Install dependencies

```
Windows: pip install -r requirements.txt
Linux: pip3 install -r requirements.txt
```

### SETUP
Run one of prepared files in pyspark_streaming/dstreams using python

If you want to capture .txt file by stream just put this files in "input_stream" directory.