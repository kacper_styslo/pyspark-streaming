# PSL
from os import getuid
from pwd import getpwuid
from time import sleep
from typing import NoReturn

# Third part
try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
except (ValueError, ModuleNotFoundError) as err:
    raise err

# Own
from pyspark_streaming import create_pyspark_operators

SS, SC, SSC, SQL_CONTEXT = create_pyspark_operators()


def dstream_on_simple_rdd(rdd_queue=None) -> NoReturn:
    for _ in range(5):
        rdd_queue += [SSC.sparkContext.parallelize([num for num in range(1, 1001)], 10)]

    input_stream = SSC.queueStream(rdd_queue)
    mapped_stream = input_stream.map(lambda num: (num % 10, 1))
    reduced_stream = mapped_stream.reduceByKey(lambda n1, n2: n1 + n2)
    reduced_stream.pprint()


if __name__ == "__main__":
    dstream_on_simple_rdd()
    SSC.start()
    sleep(6)
    SSC.stop(stopSparkContext=True, stopGraceFully=True)
