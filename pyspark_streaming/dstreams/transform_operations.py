# PSL
from os import getuid, path
from pathlib import Path
from pwd import getpwuid
from time import sleep
from typing import NoReturn

# Third part
try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
    from pyspark import RDD
except (ValueError, ModuleNotFoundError) as err:
    raise err

# Own
from pyspark_streaming import create_pyspark_operators
from pyspark_streaming.apache_log_parser import ApacheAccessLog

SS, SC, SSC, SQL_CONTEXT = create_pyspark_operators()



def get_log_dstream():
    logs_loc: str = path.join(Path(__file__).parent.parent.parent, "data", "logs")
    log_data = SSC.textFileStream(logs_loc)
    access_log_dstream = log_data.map(ApacheAccessLog.parse_from_log_line).filter(
        lambda parsed_line: parsed_line is not None
    )
    access_log_dstream.pprint(num=30)
    return access_log_dstream


def map_ip_values(rdd_logs) -> RDD:
    mapped_logs = rdd_logs.map(lambda parsed_line: (parsed_line.ip, 1))
    return mapped_logs


def union_example() -> NoReturn:
    rdd_queue = [SSC.sparkContext.parallelize([1, 2, 3]), SSC.sparkContext.parallelize([4, 5, 6])]

    nums_dstream = SSC.queueStream(rdd_queue)
    plus_one_dstream = nums_dstream.map(lambda num: num + 1)
    plus_one_dstream.pprint()

    common_rdd = SSC.sparkContext.parallelize([7, 8, 9])
    combined_dstream = nums_dstream.transform(lambda rdd: rdd.union(common_rdd))
    combined_dstream.pprint()


if __name__ == "__main__":
    transformed_access_log_dstream = get_log_dstream().transform(map_ip_values)
    transformed_access_log_dstream.pprint(num=30)
    union_example()
    SSC.start()
    SSC.awaitTermination()
    SSC.stop(stopSparkContext=True, stopGraceFully=True)
