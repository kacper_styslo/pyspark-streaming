# PSL
import re
from os import getuid, path
from pathlib import Path
from pwd import getpwuid
from time import sleep
from typing import NoReturn

# Third part
try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
    from pyspark import RDD
    from pyspark.sql import Row
    from pyspark.sql.functions import regexp_replace, col, trim, lower, desc
except (ValueError, ModuleNotFoundError) as err:
    raise err

# Own
from pyspark_streaming import create_pyspark_operators

_pyspark_operators = create_pyspark_operators()
SS, SC, SSC, SQL_CONTEXT = _pyspark_operators[0], _pyspark_operators[1], _pyspark_operators[2], _pyspark_operators[3]


def capture_text_file():
    return SSC.textFileStream(path.join(Path(__file__).parent.parent.parent, "input_stream"))


def create_rdd():
    watched_dir = capture_text_file()
    return watched_dir.flatMap(lambda line: re.split(" ", line.lower().strip()))


def show_top_five_most_common_words_in_df(rdd) -> NoReturn:
    try:
        words_rdd = rdd.filter(lambda x: len(x) > 3)
        words_rdd.collect()
        words_df = SQL_CONTEXT.createDataFrame(words_rdd.map(lambda w: (w, 1)), ["word", "count"])
        words_df.show(5)
        df_transformed = words_df.select(
            lower(trim(regexp_replace(col("word"), r'[.,\/#$%^&*()-_+=~!"\s]*', ""))).alias("word")
        )
        top_words = SQL_CONTEXT.createDataFrame(
            df_transformed.groupby("word").count().sort(desc("count")).take(5)
        )
        top_words.show()
    except ValueError:
        pass


def show_top_five_most_common_words_in_df_v2(rdd) -> NoReturn:
    try:
        row_rdd = rdd.map(lambda word: Row(word=word))
        words_df = SS.createDataFrame(row_rdd)
        words_df.createOrReplaceTempView("words")
        all_counted_words = SS.sql("SELECT word, count(*) as total FROM words GROUP BY word ORDER BY total DESC")
        all_counted_words.show()
    except ValueError:
        pass


if __name__ == "__main__":
    # create_rdd().foreachRDD(show_top_five_most_common_words_in_df)
    create_rdd().foreachRDD(show_top_five_most_common_words_in_df_v2)
    SSC.start()
    SSC.awaitTermination()
    SSC.stop(stopSparkContext=True, stopGraceFully=True)
