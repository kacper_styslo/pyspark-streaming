from os import getuid
from pwd import getpwuid

try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
    from pyspark import SparkContext
    from pyspark.streaming import StreamingContext
    from pyspark.sql import SparkSession
    from pprint import pprint
except (ValueError, ModuleNotFoundError) as err:
    raise err


def update_fnc(newValues, runningCount):
    if runningCount is None:
        runningCount = 0
    return sum(newValues, runningCount)


checkpoint_dir: str = 'checkpoint'


def create_context():
    ssc = StreamingContext(SparkContext(), 2)
    ds = ssc.textFileStream('data') \
        .map(lambda x: int(x) % 10) \
        .map(lambda x: (x, 1)) \
        .updateStateByKey(update_fnc)
    ds.pprint()
    ds.count().pprint()
    ssc.checkpoint(checkpoint_dir)
    return ssc


ssc = StreamingContext.getOrCreate(
    checkpoint_dir, create_context)
ssc.start()
