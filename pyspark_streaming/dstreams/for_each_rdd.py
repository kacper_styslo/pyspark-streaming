# PSL
from os import getuid, path
from pathlib import Path
from pwd import getpwuid
from typing import Any, Tuple, Union, Hashable, NoReturn

# Third part
try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
    from pyspark.streaming.dstream import DStream
except (ValueError, ModuleNotFoundError) as err:
    raise err

# Own
from pyspark_streaming import create_pyspark_operators

SS, SC, SSC, SQL_CONTEXT = create_pyspark_operators()

def capture_text_file():
    return SSC.textFileStream(path.join(Path(__file__).parent.parent.parent, "input_stream"))


def create_rdd():
    watched_dir = capture_text_file()
    return (
        watched_dir.flatMap(lambda row: row.split())
        .map(lambda x: (x, 1))
        .reduceByKey(lambda x, y: x + y)
    )


def show_rdd_results(rdd) -> NoReturn:
    print(rdd.take(5))


if __name__ == "__main__":
    create_rdd().foreachRDD(show_rdd_results)
    SSC.start()
    SSC.awaitTermination()
    SSC.stop(stopSparkContext=True, stopGraceFully=True)
