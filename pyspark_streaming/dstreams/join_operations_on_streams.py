# PSL
from os import getuid
from pwd import getpwuid
from typing import List

# Third part
try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
    from pyspark import RDD
    from pyspark.sql.functions import regexp_replace, col, trim, lower, desc
except (ValueError, ModuleNotFoundError) as err:
    raise err

# Own
from pyspark_streaming import create_pyspark_operators

SS, SC, SSC, SQL_CONTEXT = create_pyspark_operators()


def create_rdd() -> List[RDD]:
    rdd_queue = []
    for _ in range(5):
        rdd_data = range(100)
        rdd = SSC.sparkContext.parallelize(rdd_data)
        rdd_queue.append(rdd)
    return rdd_queue


def create_dstream():
    ds1 = SSC.queueStream(create_rdd()).map(lambda num: (num % 10, 1)).window(windowDuration=4).reduceByKey(
        lambda n1, n2: n1 + n2)
    ds1.pprint()
    ds2 = SSC.queueStream(create_rdd()).map(lambda num: (num % 5, 1)).window(windowDuration=20).reduceByKey(
        lambda n1, n2: n1 + n2)
    ds2.pprint()
    return ds1, ds2


def join_dstream_with_dstream(ds1, ds2):
    joined_stream = ds1.join(ds2)
    joined_stream.pprint()


def create_transaction_rdd_queue():
    transaction_rdd_queue = []
    for i in range(5):
        transactions = [(0, None), (1, None), (2, None), (3, None), (4, None), (5, None), (6, None), (7, None),
                        (8, None), (9, None)]
        transaction_rdd = SSC.sparkContext.parallelize(transactions)
        transaction_rdd_queue.append(transaction_rdd)
    return transaction_rdd_queue


def create_raw_rdd():
    bool_rdd = SSC.sparkContext.parallelize(
        [(0, True), (1, False), (2, True), (3, False), (4, True), (5, False), (6, True), (7, False), (8, True),
         (9, False)])
    return bool_rdd


def join_rdd_with_dstream():
    dst = SSC.queueStream(create_transaction_rdd_queue()).transform(lambda rdd: rdd.join(create_raw_rdd())).filter(
        lambda rdd: rdd[1][1] == True)
    dst.pprint()


if __name__ == "__main__":
    ds1, ds2 = create_dstream()
    join_dstream_with_dstream(ds1, ds2)
    join_rdd_with_dstream()
    SSC.start()
    SSC.stop(stopSparkContext=True, stopGraceFully=True)
