# PSL
from os import getuid
from pwd import getpwuid
from typing import NoReturn

# Third part
try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
except (ValueError, ModuleNotFoundError) as err:
    raise err

# Own
from pyspark_streaming import create_pyspark_operators

SS, SC, SSC, SQL_CONTEXT = create_pyspark_operators()



def some_transformation_examples() -> NoReturn:
    print(f"Map example: {SC.parallelize([3, 4, 5]).map(lambda num: range(1, num)).collect()}")
    print(
        f"FlatMap example: {SC.parallelize([3, 4, 5]).flatMap(lambda num: range(1, num)).collect()}"
    )

    print(
        f"Map square example: {SC.parallelize([3, 4, 5]).map(lambda num: [num, num * num]).collect()}"
    )
    print(
        f"FlatMap square example: {SC.parallelize([3, 4, 5]).flatMap(lambda num: [num, num * num]).collect()}"
    )


def find_largest_rdd_in_current_stream() -> NoReturn:
    input_stream = SSC.queueStream(
        [SC.parallelize([(1, "a"), (2, "b"), (1, "c"), (2, "d"), (1, "e"), (3, "f")], 3)]
    )
    max_num_in_stream = input_stream.reduce(max)
    max_num_in_stream.pprint()


if __name__ == "__main__":
    some_transformation_examples()
    find_largest_rdd_in_current_stream()
    SSC.start()
    SSC.awaitTermination()
    SSC.stop(stopSparkContext=True, stopGraceFully=True)
