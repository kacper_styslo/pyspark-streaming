# PSL
from os import getuid
from pwd import getpwuid
from typing import NoReturn

# Third part
try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
except (ValueError, ModuleNotFoundError) as err:
    raise err

# Own
from pyspark_streaming import create_pyspark_operators

SS, SC, SSC, SQL_CONTEXT = create_pyspark_operators()
SSC.checkpoint("checkpoint")


def window_word_count() -> NoReturn:
    lines = SSC.socketTextStream("localhost", 5555)
    words = lines.flatMap(lambda line: line.split(" "))
    pairs = words.map(lambda word: (word, 1))
    pairs.window(30, 10).pprint()  # also countByWindow, countByKeyAndWindow , reduceByKeyAndWindow


if __name__ == "__main__":
    SSC.start()
    SSC.stop(stopSparkContext=True, stopGraceFully=True)
