# PSL
import re
from typing import NoReturn


class ApacheAccessLog:
    log_entry_regex: str = '(.*?) (.*?) (.*?) \[(.*?)\] "(.*?) (.*?) (.*?)" (\d{3}) (.*?)'

    def __init__(self, *args: str) -> NoReturn:
        self.ip: str = args[0]
        self.client_identd: str = args[1]
        self.user_id: str = args[2]
        self.date_time_string: str = args[3]
        self.method: str = args[4]
        self.endpoint: str = args[5]
        self.protocol: str = args[6]
        self.response_code: str = args[7]
        self.content_size: str = args[8]

    @staticmethod
    def parse_from_log_line(logline):
        m = re.search(ApacheAccessLog.log_entry_regex, logline)
        if m is None:
            print("Cannot parse logline" + logline)
            return None
        return ApacheAccessLog(
            m.group(1),
            m.group(2),
            m.group(3),
            m.group(4),
            m.group(5),
            m.group(6),
            m.group(7),
            m.group(8),
            m.group(9),
        )

    def __repr__(self) -> str:
        return (
            f"{self.ip} {self.client_identd} {self.user_id} [{self.date_time_string}] "
            f"'{self.method} {self.endpoint} {self.protocol}' {self.response_code} {self.content_size}"
        )
